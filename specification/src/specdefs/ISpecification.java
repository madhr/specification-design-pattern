package specdefs;

public interface ISpecification {

  boolean isSatisfiedBy(Object o);
  ISpecification and(ISpecification ISpecification);
  ISpecification or(ISpecification ISpecification);
  ISpecification not(ISpecification ISpecification);
}
