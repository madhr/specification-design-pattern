package specdefs;// tworzy nowa specyfikacje, ktora jest orem dwoch innych specyfikacji

public class OrSpecification extends AbstractSpecification {

  private ISpecification spec1;
  private ISpecification spec2;

  public OrSpecification(final ISpecification spec1, final ISpecification spec2) {
    this.spec1 = spec1;
    this.spec2 = spec2;
  }

  public boolean isSatisfiedBy(final Object o) {
    return spec1.isSatisfiedBy(o) || spec2.isSatisfiedBy(o);
  }
}
