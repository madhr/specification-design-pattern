package specdefs;

// tworzy nowa specyfikacje, ktora jest andem dwoch innych specyfikacji
public class AndSpecification extends AbstractSpecification {

  private ISpecification spec1;
  private ISpecification spec2;

  public AndSpecification(final ISpecification spec1, final ISpecification spec2) {
    this.spec1 = spec1;
    this.spec2 = spec2;
  }

  public boolean isSatisfiedBy(final Object o) {
    return spec1.isSatisfiedBy(o) && spec2.isSatisfiedBy(o);
  }
}
