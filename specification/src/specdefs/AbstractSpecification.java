package specdefs;

// abstrakcyjna klasa, implementuje interfejs specyfikacji, domyslne implementacje AND, OR, NOT
public abstract class AbstractSpecification implements ISpecification {

  public abstract boolean isSatisfiedBy(Object o);

  public ISpecification and(final ISpecification ISpecification) {
    return new AndSpecification(this, ISpecification);
  }

  public ISpecification or(final ISpecification ISpecification) {
    return new OrSpecification(this, ISpecification);
  }

  public ISpecification not(final ISpecification ISpecification) {
    return new NotSpecification(ISpecification);
  }
}
