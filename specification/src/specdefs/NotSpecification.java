package specdefs;

// dekorator, tworzy nowa specyfikacje, ktora jest odwrotnoscia danej
public class NotSpecification extends AbstractSpecification {

  private ISpecification spec1;

  public NotSpecification(final ISpecification spec1) {
    this.spec1 = spec1;
  }

  public boolean isSatisfiedBy(final Object o) {
    return !spec1.isSatisfiedBy(o);
  }
}
