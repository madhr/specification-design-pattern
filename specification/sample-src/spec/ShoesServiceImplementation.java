package spec;

import domain.Color;
import domain.Country;
import domain.Shoes;
import domain.Type;
import specdefs.ISpecification;

import java.security.cert.CollectionCertStoreParameters;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Magda on 09.04.2016.
 */
public class ShoesServiceImplementation implements ShoesService {

    private ShoesRepository repo;

    @Override
    public void setRepository(final ShoesRepository repo) {
        this.repo=repo;
    }

    @Override
    public Collection<Shoes> findCandidateShoes() {
        final Collection<Shoes> keepers = new HashSet<Shoes>();
        final ISpecification colorBlue = new SpecShoesColor(Color.BLUE);
        final ISpecification properCountry = new SpecShoesOwnerCountry(getRequiredCountries()); // poland england sweden
        final ISpecification maxSize = new SpecShoesSize(41);
        final ISpecification unused = new SpecAreShoesUnused();
        final ISpecification typeBoots = new SpecShoesType(Type.BOOTS);

        // specyfikacje!
        // NAJWAZNIEJSZA LINIJKA!!!!!!!!!!!!!!!!!!!!!!
        final ISpecification candidateShoes1 = maxSize;

        final Collection<Shoes> shoes = repo.findAllShoesInStock();

        for (Shoes s: shoes){
            if(candidateShoes1.isSatisfiedBy(s))
                keepers.add(s);
        }
        return keepers;
    }

    private Set<Country> getRequiredCountries(){
        Set<Country> reqCountries = new HashSet<Country>();
        reqCountries.add(Country.POLAND);
        reqCountries.add(Country.ENGLAND);
        reqCountries.add(Country.SWEDEN);

        return reqCountries;
    }
}
