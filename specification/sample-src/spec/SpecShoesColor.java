package spec;

import domain.Color;
import domain.Shoes;
import specdefs.AbstractSpecification;

/**
 * Created by Magda on 09.04.2016.
 */
public class SpecShoesColor extends AbstractSpecification {

    private Color color;

    public SpecShoesColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean isSatisfiedBy(Object o) {
        if(o instanceof Shoes){
            Shoes shoes = (Shoes) o;
            return shoes.getColor() == color;
        } else {
            throw new ClassCastException("Expected: shoes, given: " +o.getClass().getCanonicalName());
        }
    }
}
