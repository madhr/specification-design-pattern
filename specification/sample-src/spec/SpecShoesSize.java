package spec;


import domain.Shoes;
import domain.Size;
import specdefs.AbstractSpecification;

/**
 * Created by Magda on 09.04.2016.
 */
public class SpecShoesSize extends AbstractSpecification {

    private int size;

    public SpecShoesSize(int size) {
        this.size = size;
    }

    @Override
    public boolean isSatisfiedBy(Object o) {
        if (o instanceof Shoes){
            Shoes shoes =(Shoes) o;
            // czy rozmiar jest mniejszy/rowny
            return shoes.getSize()<=size;
        } else {
            throw new ClassCastException("Expected: shoes, given: " +o.getClass().getCanonicalName());
        }
    }
}
