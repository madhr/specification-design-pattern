package spec;

import domain.Shoes;
import tests.ShoeFactoryTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Magda on 09.04.2016.
 */
//public interface ShoesRepository {
//
//    // daj wszystkie buty
//    public Collection<Shoes> findAllShoesInStock();
//}

public interface ShoesRepository{

    public Collection<Shoes> findAllShoesInStock();
}