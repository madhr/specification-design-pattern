package spec;

import domain.Shoes;
import specdefs.AbstractSpecification;

/**
 * Created by Magda on 09.04.2016.
 */
public class SpecAreShoesUnused extends AbstractSpecification{

    @Override
    public boolean isSatisfiedBy(Object o) {
        if (o instanceof Shoes){
            Shoes shoes =(Shoes) o;
            return shoes.areUnused();
        } else {
            throw new ClassCastException("Expected: shoes, given: " +o.getClass().getCanonicalName());
        }
    }
}
