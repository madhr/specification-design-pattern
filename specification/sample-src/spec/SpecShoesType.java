package spec;

import domain.Shoes;
import domain.Type;
import specdefs.AbstractSpecification;

/**
 * Created by Magda on 09.04.2016.
 */
public class SpecShoesType extends AbstractSpecification {

    private Type type;

    public SpecShoesType(Type type) {
        this.type = type;
    }

    @Override
    public boolean isSatisfiedBy(Object o) {
        if (o instanceof Shoes){
            Shoes shoes =(Shoes) o;
            // czy rozmiar jest mniejszy/rowny
            return shoes.getType()==type;
        } else {
            throw new ClassCastException("Expected: shoes, given: " +o.getClass().getCanonicalName());
        }
    }
}
