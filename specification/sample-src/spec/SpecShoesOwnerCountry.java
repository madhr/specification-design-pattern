package spec;

import domain.Country;
import domain.Shoes;
import specdefs.AbstractSpecification;

import java.util.Set;

/**
 * Created by Magda on 09.04.2016.
 */
public class SpecShoesOwnerCountry extends AbstractSpecification {

    private Set<Country> reqCountries;


    public SpecShoesOwnerCountry(Set<Country> reqCountries) {
        this.reqCountries = reqCountries;
    }


    @Override
    public boolean isSatisfiedBy(Object o) {
        if (o instanceof Shoes){
            Shoes shoes =(Shoes) o;
            // czy lista krajow zawiera kraj wlasciciela tych butow
            return reqCountries.contains(shoes.getOwner().getAddress().getCountry());
        } else {
            throw new ClassCastException("Expected: shoes, given: " +o.getClass().getCanonicalName());
        }
    }
}
