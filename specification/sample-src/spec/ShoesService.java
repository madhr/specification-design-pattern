package spec;

import domain.Shoes;

import java.util.Collection;

/**
 * Created by Magda on 09.04.2016.
 */
public interface ShoesService {

    // utworz repo
    void setRepository(ShoesRepository repo);

    // znajdz kandydatow
    Collection<Shoes> findCandidateShoes();
}
