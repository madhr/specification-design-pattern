package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public enum Country {

    POLAND,
    ENGLAND,
    GERMANY,
    FRANCE,
    SWEDEN
}
