package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public class Address {

    Country country;


    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String toString(){
        return ""+country;
    }
}
