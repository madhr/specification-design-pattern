package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public class Person {

    private Address address;

    public Address getAddress(){
        return address;
    }

    public void setAddress(Address address){
        this.address=address;
    }

    public String toString(){
        return "owner's address: "+address;
    }
}
