package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public enum Color {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    BLACK,
    WHITE
}
