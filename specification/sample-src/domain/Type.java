package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public enum Type {
    BOOTS,
    SANDALS,
    HIGH_HEELS
}
