package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public enum Size {
    SIZE36,
    SIZE37,
    SIZE38,
    SIZE39,
    SIZE40,
    SIZE41,
    SIZE42,
    SIZE43,
    SIZE44
}
