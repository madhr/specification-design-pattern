package domain;

/**
 * Created by Magda on 09.04.2016.
 */
public class Shoes {

    private Color color;
    private Person owner;
    private Type type;
    private int size;
    private boolean areUnused;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean areUnused() {
        return areUnused;
    }

    public void setAreUnused(boolean areUnused) {
        this.areUnused = areUnused;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "\nShoes: " +
                "are new: " + areUnused +
                "|| color: " + color +
                "|| owner: " + owner +
                "|| size: " + size +
                "|| type: " + type;
    }


}
