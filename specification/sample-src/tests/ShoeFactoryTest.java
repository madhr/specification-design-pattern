package tests;

import domain.*;

/**
 * Created by Magda on 09.04.2016.
 */
public class ShoeFactoryTest {

    public static Shoes createShoes1(){
        Shoes s1 = new Shoes();
        s1.setAreUnused(true); // czy uzywane
        s1.setColor(Color.WHITE); // kolor
        s1.setSize(39); // rozmiar
        s1.setType(Type.BOOTS); // typ
        Address s1OwnerAddr = new Address();
        Person s1Owner = new Person();
        s1OwnerAddr.setCountry(Country.ENGLAND); // kraj
        s1Owner.setAddress(s1OwnerAddr);
        s1.setOwner(s1Owner); // wlasciciel
        return s1;
    }

    public static Shoes createShoes2(){
        Shoes s2 = new Shoes();
        s2.setAreUnused(false); // czy uzywane
        s2.setColor(Color.BLUE); // kolor
        s2.setSize(37); // rozmiar
        s2.setType(Type.HIGH_HEELS); // typ
        Address s2OwnerAddr = new Address();
        Person s2Owner = new Person();
        s2OwnerAddr.setCountry(Country.POLAND); // kraj
        s2Owner.setAddress(s2OwnerAddr);
        s2.setOwner(s2Owner); // wlasciciel
        return s2;
    }

    public static Shoes createShoes3(){
        Shoes s3 = new Shoes();
        s3.setAreUnused(true); // czy uzywane
        s3.setColor(Color.RED); // kolor
        s3.setSize(41); // rozmiar
        s3.setType(Type.SANDALS); // typ
        Address s3OwnerAddr = new Address();
        Person s3Owner = new Person();
        s3OwnerAddr.setCountry(Country.GERMANY); // kraj
        s3Owner.setAddress(s3OwnerAddr);
        s3.setOwner(s3Owner); // wlasciciel
        return s3;
    }

    public static Shoes createShoes4(){
        Shoes s4 = new Shoes();
        s4.setAreUnused(true); // czy uzywane
        s4.setColor(Color.BLACK); // kolor
        s4.setSize(44); // rozmiar
        s4.setType(Type.BOOTS); // typ
        Address s4OwnerAddr = new Address();
        Person s4Owner = new Person();
        s4OwnerAddr.setCountry(Country.FRANCE); // kraj
        s4Owner.setAddress(s4OwnerAddr);
        s4.setOwner(s4Owner); // wlasciciel
        return s4;
    }

    public static Shoes createShoes5(){
        Shoes s5 = new Shoes();
        s5.setAreUnused(false); // czy uzywane
        s5.setColor(Color.BLUE); // kolor
        s5.setSize(39); // rozmiar
        s5.setType(Type.BOOTS); // typ
        Address s5OwnerAddr = new Address();
        Person s5Owner = new Person();
        s5OwnerAddr.setCountry(Country.POLAND); // kraj
        s5Owner.setAddress(s5OwnerAddr);
        s5.setOwner(s5Owner); // wlasciciel
        return s5;
    }

}
