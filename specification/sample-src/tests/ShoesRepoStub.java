package tests;

import domain.Shoes;
import spec.ShoesRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Magda on 09.04.2016.
 */
 class ShoesRepoStub implements ShoesRepository {

    @Override
    public Collection<Shoes> findAllShoesInStock() {

        List<Shoes> shoes = new ArrayList<Shoes>();

        // true, white, 39, boots, england
        Shoes s1 = ShoeFactoryTest.createShoes1();
        // false, blue, 37, hh, poland
        Shoes s2 = ShoeFactoryTest.createShoes2();
        // true, red, 41, sandals, germany
        Shoes s3 = ShoeFactoryTest.createShoes3();
        // true, black, 44, boots, france
        Shoes s4 = ShoeFactoryTest.createShoes4();
        // false, blue, 39, boots, poland
        Shoes s5 = ShoeFactoryTest.createShoes5();

        shoes.add(s1);
        shoes.add(s2);
        shoes.add(s3);
        shoes.add(s4);
        shoes.add(s5);

        return shoes;
    }
}