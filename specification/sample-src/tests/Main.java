package tests;


import domain.Shoes;
import spec.ShoesService;
import spec.ShoesServiceImplementation;

import java.util.Collection;

/**
 * Created by Magda on 09.04.2016.
 */
public class Main {

    public static void main(String[] args){
        System.out.println("ALL SHOES LIST: ");
        ShoesRepoStub srs = new ShoesRepoStub();
        Collection<Shoes> available = srs.findAllShoesInStock();
        System.out.print(available.toString());

        System.out.println("\nSHOES SATISFYING CONSTRAINTS: ");
        ShoesServiceImplementation ssi = new ShoesServiceImplementation();
        ssi.setRepository(new ShoesRepoStub());
        Collection<Shoes> candShoes = ssi.findCandidateShoes();
        System.out.print(candShoes.toString());
    }

}
